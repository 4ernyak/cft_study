package ru.cft.fs.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        LOG.info("Program start!");
        FileHelper fileHelper = new FileHelper();
        Figure newFigure;

        if (args == null || args.length < 2) {
            LOG.info("You have not provided arguments.");
            return;
        }

        List<String> newFigureData = fileHelper.readInFile(args[0]);
        String[] newParameters = newFigureData.get(1).split(" ");
        Double[] newFigureParameters = new Double[newParameters.length];

        for (int i = 0; i < newParameters.length; i++) {
            try {
                newFigureParameters[i] = Double.parseDouble(newParameters[i]);
            } catch (NumberFormatException e) {
                LOG.error("Unknown format figures parameters!");
                return;
            }
        }

        switch (newFigureData.get(0).toLowerCase()) {
            case ("triangle"):
                newFigure = new Triangle(newFigureParameters);
                break;
            case ("rectangle"):
                newFigure = new Rectangle(newFigureParameters);
                break;
            case ("circle"):
                newFigure = new Circle(newFigureParameters);
                break;
            default:
                LOG.error("Unknown figures!");
                return;
        }

        if (args[1].equals("-c")) {
            System.out.println(newFigure);
        } else if (args[1].equals("-f")) {
            fileHelper.writeOutTxtFile(newFigure, args[2]);
        }

        LOG.info("Program end!");
    }
}
