package ru.cft.fs.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileHelper {

    private static final Logger LOG = LoggerFactory.getLogger(FileHelper.class);

    public List<String> readInFile(String fileName) {

        BufferedReader bufferedReader = null;
        List<String> inputData = new ArrayList<>();

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                inputData.add(line);
            }
        } catch (IOException e) {
            LOG.error("Input file not found, program end!");
            System.exit(0);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return inputData;
    }

    public void writeOutTxtFile(Object figure, String fileName) {

        BufferedWriter bufferedWriter = null;

        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(".\\data\\out\\" + fileName), "UTF-8"));
            bufferedWriter.write(figure.toString());
            bufferedWriter.write(System.lineSeparator());
        } catch (IOException e) {
            LOG.error("Output file can't write, program end!");
            System.exit(0);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
