package ru.cft.fs.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Circle implements Figure {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static final String NAME = "Круг";
    private double radius;

    public Circle(Double... args) {
        switch (args.length) {
            case (1):
                createFigure(args[0]);
                break;
            default:
                LOG.error("Incorrect figures parameters count!");
        }
    }

    private void createFigure(double radius) {
        this.radius = radius;
    }

    public double calculateArea() {
        return 3.1415 * radius * radius;
    }

    public double calculatePerimeter() {
        return 2 * 3.1415 * radius;
    }

    public double calculateDiameter() {
        return radius * 2;
    }

    @Override
    public String toString() {
        return NAME + "\n" +
                "Площадь = " + calculateArea() + "\n" +
                "Периметр = " + calculatePerimeter() + "\n" +
                "Радиус = " + radius + "\n" +
                "Диаметр = " + calculateDiameter();
    }
}
