package ru.cft.fs.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Triangle implements Figure {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static final String NAME = "Треугольник";
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(Double... args) {
        switch (args.length){
            case (1):
                createFigure(args[0]);
                break;
            case (2):
                createFigure(args[0], args[1]);
                break;
            case (3):
                createFigure(args[0], args[1], args[2]);
                break;
            default:
                LOG.error("Incorrect figures parameters count!");
        }
    }

    private void createFigure(Double sideA, Double sideB, Double sideC) {

        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    private void createFigure(Double sideA, Double sideBAndC) {

        this.sideA = sideA;
        this.sideB = sideBAndC;
        this.sideC = sideBAndC;
    }

    private void createFigure(Double allSide) {

        this.sideA = allSide;
        this.sideB = allSide;
        this.sideC = allSide;
    }

    public double calculateAngle(double s1, double s2, double s3) {
        return Math.acos((Math.pow(s2, 2) + Math.pow(s3, 2) - Math.pow(s1, 2)) / (2 * s2 * s3)) * 180 / 3.1415;
    }

    @Override
    public double calculateArea() {

        double halfPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC));
    }

    @Override
    public double calculatePerimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public String toString() {

        return NAME + "\n" +
                "Площадь = " + calculateArea() + "\n" +
                "Периметр = " + calculatePerimeter() + "\n" +
                "Сторона А = " + sideA + " , угол = " + calculateAngle(sideA, sideB, sideC) + "\n" +
                "Сторона B = " + sideB + " , угол = " + calculateAngle(sideB, sideA, sideC) + "\n" +
                "Сторона С = " + sideC + " , угол = " + calculateAngle(sideC, sideA, sideB);
    }
}
