package ru.cft.fs.task3;

public interface Figure {

    double calculateArea();
    double calculatePerimeter();

}
