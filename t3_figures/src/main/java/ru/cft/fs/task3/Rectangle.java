package ru.cft.fs.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Rectangle implements Figure {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static final String NAME = "Прямоугольник";
    private double longSide;
    private double shortSide;

    public Rectangle(Double... args) {
        switch (args.length){
            case (1):
                createFigure(args[0]);
                break;
            case (2):
                createFigure(args[0], args[1]);
                break;
            default:
                LOG.error("Incorrect figures parameters count!");
        }
    }

    private void createFigure(double sideA, double sideB) {

        if (sideA > sideB) {
            this.longSide = sideA;
            this.shortSide = sideB;
        } else {
            this.longSide = sideB;
            this.shortSide = sideA;
        }
    }

    private void createFigure(double side) {

        this.longSide = side;
        this.shortSide = side;
    }


    public double calculateDiagonal() {
        return Math.sqrt(Math.pow(longSide, 2) + Math.pow(shortSide, 2));
    }

    @Override
    public double calculateArea() {
        return longSide * shortSide;
    }

    @Override
    public double calculatePerimeter() {
        return (longSide + shortSide) * 2;
    }

    @Override
    public String toString() {

        return NAME + "\n" +
                "Площадь = " + calculateArea() + "\n" +
                "Периметр = " + calculatePerimeter() + "\n" +
                "Диагональ = " + calculateDiagonal() + "\n" +
                "Длина = " + longSide + "\n" +
                "Ширина = " + shortSide;
    }
}
