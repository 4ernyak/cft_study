package ru.cft.fs.t6.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import java.awt.*;
import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class Client {

    private static final Logger LOG = LoggerFactory.getLogger(Client.class);
    private BufferedReader in;
    private PrintWriter out;
    private String chatterName;
    private JFrame frame = new JFrame("Chat!");
    private JTextField textField = new JTextField(30);
    private JTextArea messageArea = new JTextArea(8, 40);
    private Socket socket;

    private String serverAddress;

    public Client() {

        GridBagLayout layout = new GridBagLayout();
        frame.setLayout(layout);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 3;
        frame.add(new JScrollPane(messageArea), constraints);
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        frame.add(textField, constraints);
        textField.setEditable(false);
        messageArea.setEditable(false);
        DefaultCaret defaultCaret = (DefaultCaret) messageArea.getCaret();
        defaultCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        constraints.gridx = 2;
        constraints.gridwidth = 1;
        constraints.gridy = 2;
        JButton sendMessageButton = new JButton("Send!");
        frame.add(sendMessageButton, constraints);
        frame.setResizable(false);
        frame.pack();

        sendMessageButton.addActionListener(e -> {
            out.println(textField.getText());
            textField.setText("");
        });

        textField.addActionListener(e -> {
            out.println(textField.getText());
            textField.setText("");
        });
    }


    public static void main(String[] args) throws Exception {
        LOG.info("Client running");
        Client client = new Client();
        client.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        client.frame.setVisible(true);
        client.run();
    }

    private void run() throws IOException {

        connectToServer();
        LOG.info("Connect to server " + serverAddress);

        while (true) {
            try {
                String line = in.readLine();
                if (line.startsWith("SUBMITNAME")) {
                    out.println(getName());
                } else if (line.startsWith("NAMEACCEPTED")) {
                    textField.setEditable(true);
                    frame.setTitle("Chat! Your name is " + chatterName);
                    LOG.info("Your name " + chatterName + " accepted");
                } else if (line.startsWith("MESSAGE")) {
                    messageArea.append(line.substring(8) + "\n");
                }
            } catch (SocketException e) {
                LOG.info("Disconnect from server");
                JOptionPane.showMessageDialog(frame, "Disconnect from server.");
                connectToServer();
            }
        }
    }

    private String getServerAddress() {
        return JOptionPane.showInputDialog(
                frame,
                "Enter IP Address of the Server:",
                "Welcome to the Chat!",
                JOptionPane.QUESTION_MESSAGE);
    }

    private String getName() {
        this.chatterName = JOptionPane.showInputDialog(
                frame,
                "Choose a screen name:",
                "Screen name selection",
                JOptionPane.PLAIN_MESSAGE);
        return chatterName;
    }

    private void connectToServer() throws IOException {
        if (serverAddress == null) {
            serverAddress = getServerAddress();
        }
        socket = getSocket(serverAddress);
        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    private Socket getSocket(String serverAddress) {
        socket = null;

        while (socket == null) {
            try {
                socket = new Socket(serverAddress, 9001);
            } catch (IOException e) {
                messageArea.append("Server is not available, reconnect after 5 seconds" + System.lineSeparator());
                LOG.info("Server is not available, reconnect after 5 seconds");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return socket;
    }
}