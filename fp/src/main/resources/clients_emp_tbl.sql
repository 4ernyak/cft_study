CREATE TABLE emp_tbl
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    email VARCHAR(45) NOT NULL,
    address VARCHAR(45) NOT NULL,
    telephone VARCHAR(45) NOT NULL
);

INSERT INTO clients.emp_tbl (name, email, address, telephone) VALUES ('Петров', 'petrov@hospit.ru', 'asd', '890112345677');
INSERT INTO clients.emp_tbl (name, email, address, telephone) VALUES ('Иванов', 'ivan@mail.ru', 'asd', '899945612377');
INSERT INTO clients.emp_tbl (name, email, address, telephone) VALUES ('Васильев', 'vvasilev@cft.ru', 'street', '845654564448');
INSERT INTO clients.emp_tbl (name, email, address, telephone) VALUES ('Николаев', 'nikola@mail.ru', 'Novosibirsk', '+79874564152');