package ru.cft.fs.fp.service;

import java.util.List;

import ru.cft.fs.fp.model.Employee;

public interface EmployeeService {
	
	public void addEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public void deleteEmployee(Integer employeeId);

	public Employee getEmployee(int employeeid);

	public Employee updateEmployee(Employee employee);

	public List<Employee> getEmployees(String employeeName);
}
