package ru.cft.fs.fp.dao;

import java.util.List;
import ru.cft.fs.fp.model.Employee;

public interface EmployeeDAO {

	public void addEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public void deleteEmployee(Integer employeeId);

	public Employee updateEmployee(Employee employee);

	public Employee getEmployee(int employeeid);

    public List getEmployees(String name);
}
