package ru.cft.fs.task4;

import java.util.ArrayList;
import java.util.List;

class Controller {

    private static int lengthMineField;
    private static int widthMineField;
    private static boolean[][] mineField = new boolean[widthMineField][lengthMineField];

    Controller(int length, int width) {
        lengthMineField = length;
        widthMineField = width;
        mineField = new boolean[width][length];
    }

    static void generateMine(int mineCount) {
        for (int i = 0; i < mineCount; i++) {
            int newMine = (int) (Math.random() * (lengthMineField * widthMineField) - 1);
            int y = newMine % widthMineField;
            int x = newMine / lengthMineField;
            if (!checkMine(x, y)) {
                mineField[x][y] = true;
            } else {
                i--;
            }
        }
    }

    static boolean checkMine(int x, int y) {
        boolean mine = false;
        if (mineField[x][y]) {
            mine = true;
        }
        return mine;
    }

    static int checkCountBombAround(int x, int y) {
        int count = 0;
        List<Boolean> aroundMineList = new ArrayList<>();
        if (x > 0) {
            aroundMineList.add(mineField[x - 1][y]);
        }
        if (y > 0) {
            aroundMineList.add(mineField[x][y - 1]);
        }
        if (x > 0 && y > 0) {
            aroundMineList.add(mineField[x - 1][y - 1]);
        }
        if (x < widthMineField - 1) {
            aroundMineList.add(mineField[x + 1][y]);
        }
        if (y < lengthMineField - 1) {
            aroundMineList.add(mineField[x][y + 1]);
        }
        if (x < widthMineField - 1 && y < lengthMineField - 1) {
            aroundMineList.add(mineField[x + 1][y + 1]);
        }
        if (x < widthMineField - 1 && y > 0) {
            aroundMineList.add(mineField[x + 1][y - 1]);
        }
        if (x > 0 && y < lengthMineField - 1) {
            aroundMineList.add(mineField[x - 1][y + 1]);
        }
        for (Boolean cell : aroundMineList) {
            if (cell) {
                count++;
            }
        }
        return count;
    }


}
