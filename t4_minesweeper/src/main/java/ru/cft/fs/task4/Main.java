package ru.cft.fs.task4;

/**
 * Created by Starscrim3 on 13.03.2017.
 */
public class Main {

    static int lengthMineField;
    static int widthMineField;
    static int mineCount;
    static int lengthWindow;
    static int widthWindow;

    static void startGame() {

        MainFrame myFrame = new MainFrame(lengthMineField, widthMineField, mineCount);
        myFrame.setVisible(true);
        myFrame.setSize(lengthWindow, widthWindow);
        myFrame.setResizable(false);
    }

    static void newGame() {

        SettingsFrame settingsFrame = new SettingsFrame();
        settingsFrame.setVisible(true);
        settingsFrame.setSize(300, 100);
        settingsFrame.setResizable(false);
    }

    public static void main(String[] args) {

        newGame();
    }
}
