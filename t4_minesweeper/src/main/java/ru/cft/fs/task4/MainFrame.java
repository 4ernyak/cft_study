package ru.cft.fs.task4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

class MainFrame extends JFrame {

    private int openCell = 0;
    private boolean gameRun = false;
    private JTextField timer = new JTextField("0");
    private JTextField mineCountField = new JTextField();
    private int mineCount = 10;
    private Date startGame;

    MainFrame(int length, int width, int mines) {

        mineCount = mines;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        GridBagLayout layout = new GridBagLayout();
        setLayout(layout);
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 2;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 0;
        c.gridx = 0;
        c.gridy = 0;
        setJMenuBar(getMyMenuBar());
        Button[][] buttonArray = new Button[length][width];
        new Controller(length, width);
        Controller.generateMine(mineCount);

        for (int i = 0; i < length; i++) {
            c.gridx = i;
            for (int j = 0; j < width; j++) {
                c.gridy = j;
                buttonArray[i][j] = new Button(i, j);
                buttonArray[i][j].setMargin(new Insets(0, 0, 0, 0));
                buttonArray[i][j].setPreferredSize(new Dimension(20, 20));
                add(buttonArray[i][j], c);
                buttonArray[i][j].addMouseListener(new MouseAdapter() {

                    @Override
                    public void mousePressed(MouseEvent e) {
                        if (!gameRun) {
                            startGame = new Date();
                        }
                        startTimer();
                        gameRun = true;
                        if (e.getButton() == MouseEvent.BUTTON1) {
                            Button button = (Button) e.getSource();
                            if (button.getIcon() == null) {
                                if (Controller.checkMine(button.j, button.i)) {
                                    Object[] options = {"ok", "quit"};
                                    gameRun = false;
                                    int choice = JOptionPane.showOptionDialog(MainFrame.this, "You loose the game :(, restart?", "Game Over", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                                    if (choice == JOptionPane.NO_OPTION) {
                                        System.exit(0);
                                    }
                                    if (choice == JOptionPane.YES_OPTION) {
                                        MainFrame.this.dispose();
                                        Main.startGame();
                                    }
                                } else {
                                    int bombAroundCount = Controller.checkCountBombAround(button.j, button.i);
                                    button.setEnabled(false);
                                    openCell++;
                                    if (bombAroundCount != 0) {
                                        button.setText("" + bombAroundCount);
                                    }
                                    if (bombAroundCount == 0) {
                                        openZero(button.j, button.i);
                                    }
                                    if (openCell == (length * width - mines)) {
                                        Object[] options = {"ok", "quit"};
                                        gameRun = false;
                                        int choice = JOptionPane.showOptionDialog(MainFrame.this, "You win :) your result " + timer.getText() + "s, restart?", "Game Over", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                                        if (choice == JOptionPane.NO_OPTION) {
                                            System.exit(0);
                                        }
                                        if (choice == JOptionPane.YES_OPTION) {
                                            MainFrame.this.dispose();
                                            Main.startGame();
                                        }
                                    }
                                }
                            }
                        } else if (e.getButton() == MouseEvent.BUTTON3) {
                            Button button = (Button) e.getSource();
                            ImageIcon flag = new ImageIcon("flag.gif");
                            if (button.isEnabled()) {
                                if (button.getIcon() == null) {
                                    button.setIcon(flag);
                                    MainFrame.this.mineCount--;
                                    mineCountField.setText(MainFrame.this.mineCount + "");
                                } else {
                                    button.setIcon(null);
                                    MainFrame.this.mineCount++;
                                    mineCountField.setText(MainFrame.this.mineCount + "");
                                }
                            }
                        }
                    }

                    private void openZero(int y, int x) {

                        for (int k = x - 1; k <= x + 1; k++) {
                            for (int l = y - 1; l <= y + 1; l++) {
                                if (k < 0 || k > length - 1 || l < 0 || l > width - 1) {
                                    continue;
                                }
                                if (buttonArray[k][l].isEnabled()) {
                                    buttonArray[k][l].setEnabled(false);
                                    openCell++;
                                    int bombCount = Controller.checkCountBombAround(l, k);
                                    if (bombCount != 0) {
                                        buttonArray[k][l].setText("" + bombCount);
                                    }
                                    if (bombCount == 0) {
                                        openZero(l, k);
                                    }
                                }

                            }
                        }
                    }
                });
            }
        }
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weighty = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 18;
        c.gridwidth = 2;
        timer.setText("0");
        add(timer, c);
        c.gridx = 6;
        c.gridy = 18;
        c.gridwidth = 2;
        mineCountField.setText(mineCount + "");
        add(mineCountField, c);
    }

    private void startTimer() {
        Thread timerThread = new Thread(() -> {
            while (gameRun) {
                Long timeInGame = (new Date().getTime() - startGame.getTime()) / 1000;
                timer.setText(timeInGame.toString());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        timerThread.start();
    }

    private JMenuBar getMyMenuBar() {
        Font font = new Font("Verdana", Font.PLAIN, 14);
        JMenuBar menuBar = new JMenuBar();
        menuBar.setFont(font);
        JMenu jMenu = new JMenu("File");
        menuBar.add(jMenu);

        JMenuItem newGame = new JMenuItem("Nem Game");
        JMenuItem exit = new JMenuItem("Exit");
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                Main.newGame();
            }
        });
        jMenu.addSeparator();
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        jMenu.add(newGame);
        jMenu.add(exit);
        return menuBar;
    }

}

