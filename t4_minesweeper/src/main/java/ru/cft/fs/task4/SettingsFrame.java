package ru.cft.fs.task4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingsFrame extends JFrame {

    public SettingsFrame() {
        JPanel jPanel = new JPanel(new BorderLayout());

        JRadioButton noviceLevelSettings = new JRadioButton("Novice - 9*9 with 10 mines");
        noviceLevelSettings.setSelected(true);

        JRadioButton advancedLevelSettings = new JRadioButton("Advanced - 16*16 with 40 mines");

        ButtonGroup settingsGroup = new ButtonGroup();
        settingsGroup.add(noviceLevelSettings);
        settingsGroup.add(advancedLevelSettings);

        JButton okButton = new JButton("Ok");

        JPanel radioPanel = new JPanel(new GridLayout(0, 1));
        radioPanel.add(noviceLevelSettings);
        radioPanel.add(advancedLevelSettings);
        radioPanel.add(okButton);

        jPanel.add(radioPanel, BorderLayout.LINE_START);

        add(jPanel);

        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (noviceLevelSettings.isSelected()) {
                    Main.lengthMineField = 9;
                    Main.widthMineField = 9;
                    Main.mineCount = 10;
                    Main.lengthWindow = 300;
                    Main.widthWindow = 300;
                    SettingsFrame.this.dispose();
                    Main.startGame();
                } else if (advancedLevelSettings.isSelected()) {
                    Main.lengthMineField = 16;
                    Main.widthMineField = 16;
                    Main.mineCount = 40;
                    Main.lengthWindow = 500;
                    Main.widthWindow = 450;
                    SettingsFrame.this.dispose();
                    Main.startGame();
                }
            }
        });
    }
}
