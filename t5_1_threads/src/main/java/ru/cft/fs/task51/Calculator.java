package ru.cft.fs.task51;

import java.util.concurrent.Callable;

public class Calculator implements Callable<Long> {

    private int numberStart;
    private int numberEnd;

    public Calculator(int numberStart, int numberEnd) {
        this.numberStart = numberStart;
        this.numberEnd = numberEnd;
    }

    public Long call() {
        long sum = 0;
        for (int i = numberStart; i < numberEnd; i++) {
            int delimCount = 0;
            for (int k = 1; k < i / 2; k++) {
                if (i % k == 0) {
                    delimCount++;
                }
            }
            if (delimCount < 2) {
                sum += i;
            }
        }
        System.out.println(sum);
        return sum;
    }
}
