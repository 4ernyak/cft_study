package ru.cft.fs.task51;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();

        List<Future<Long>> futureList = new ArrayList<>();
        ExecutorService pool = Executors.newFixedThreadPool(4);
        for (int i = 1; i < 400000; i += 100000) {
            Callable<Long> callable = new Calculator(i, i + 100000);
            Future<Long> task = pool.submit(callable);
            futureList.add(task);
        }

        long sum = 0;

        for (Future<Long> future : futureList) {
            try {
                sum += future.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        pool.shutdown();
        System.out.println(sum);

        long finish = System.currentTimeMillis();
        System.out.println((finish - start) / 1000);
    }
}
